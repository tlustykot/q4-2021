This is a simple project that is a playground for recruitment purposes.

## Getting Started

This project consists of an API server and a frontend website - you can find them
in `api` and `front` folders.

## Running API locally

1. Go into `api` folder (`cd api`) and run `npm install` to get all packages.
2. Run API with `npm run start`.

If everything went fine you should see `API is running and listening on: 8001`.

However if you do not see this, first check if port 8001 is not blocked on your computer.
If there are any other problems please contact anyone who is recruiting you via email before
live-coding session starts.

API endpoints are available at `localhost:8001/api/v1/`.

## Running website

1. Open another terminal and this time go into `front` folder (`cd front`) and run `npm install` to get all packages.
2. Start website server with `npm run dev`.

If everything went fine you should see `compiled succesfully` in your terminal.

Website should be accessible at `localhost:3000` in your browser.

## Ready for work

If you open `localhost:3000`, you should see:
_10 users fetched from API._
_Everything is fine. Good luck._

It means that frontend can make requests to API and that everything is fine
with project installation.

## Create your branch

If you are starting your task please create new branch in `tasks/firstname-lastname`.

## Live-coding or time-attack

If your recruitment process is live-coding session then make sure you have this project up and running
before joining a call with TFC. Otherwise you will lose your time that is meant for a task.

If your were given this repo as a time-attack task, just let us know that you are ready
and we will email you your task. You need to provide a pull request with your branch
to `hr-results` branch, that way we can comment and easily evaulate your code. Make sure to submit
your pull request in a given time limit.

## Code guidelines for recruitment tasks

1. We use camelCase in our code.
2. In API you should code all business logic in services only.
3. Validation and sanitazation should be taken care in controllers.
4. Routes are only meant to define API endpoints and run controllers function and middlewares.
5. Comment your code. If there is anything more complicated make sure to describe how exactly it works.
6. Don't leave any commented/inactive parts in final code.
7. Remove all console.logs from final code.

## API code structure

1. Routes - in routes/v1 (or v2, v3 and so on) we define all endpoints' routes. All of them have to
   be imported in routes/v1/index.js. Each resource (users, appointments, forms, etc.) have to be named properly
   and their routes have to be in separate files. If there are multiple ways of getting each resource - for
   example a public version of some service, then it should be put into other folder routes/v1/public. Also
   we tend to provide middlewares for account privileges system.
2. Controllers - controllers are fired by routes. We try to keep same naming in controllers as we do for routes.
   Each route should almost always have its separate controller. Same case for public spaces. In controller we use
   express-validator to validate query and body params. If service throws custom error we put it into try/catch here
   and map thrown errors to HTTP errors (401, 403, 404, 500, etc). If endpoint returns 422 it's considered a bad code.
3. Services - service methods are fired by controllers. Services contain business logic. They are mostly based on
   resources - for example service for user processing - UsersService. There always is an index file to define all methods
   that service provides.
4. Middlewares - these are middlewares that are put into code somewhere between routes and controllers or are used by
   `app.use`.
5. Helpers - any shortcodes that do specific object manipulation and can be used throughout the project.
6. Exceptions - place to define exceptions - used for HTTP error throwing

## HR project info

This project was created only for recruitment process and is stripped of any task-irrelevant things
such as database handler, request handlers, authentication services, etc.

It does not need any database - objects are mocked in models instead of getting
them by a query.

It does not represent code quality provided by TFC.

Its main purpose is candidate knowledge evaulation.

We know that .env files should not be included in repo :)

API structure consists of routes, controllers, services and models.
Routes define API endpoints that are accessible via http.
Controllers are meant to validate and run services required by endpoint routes.
Services handle all specific jobs and process objects from db.
Models run db queries. Normally `models/` have separate files for each db table and each method
consists only of queries to the db.

Frontend structure consists of components, pages, public, scss, hooks and services.
Components is a folder where all components should be stored.
Pages is where all separate pages should be created. It uses Next.js mechanism of creating urls from folder structure.
Public is where you should put all images and other assets.
Scss stores all scss files.
Services is meant for API handling classes, translation services or any other piece of code that does not belong in components.
Hooks is where you should define any custom hooks.

Remember that not completing your recruitment task doesn't have to mean that you won't get a job.
We only want to see how you handle code, how you work and most importantly how you think.

## Good luck!
