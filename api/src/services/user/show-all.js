/** @format */
const UserModel = require("@models/user-mocked.model");

class ShowAll {
  /**
   * Gets a list of all users
   */
  async execute() {
    const usersList = UserModel.findAll();
    return usersList;
  }
}

module.exports = ShowAll;
