/** @format */

const { validationResult } = require("express-validator");
const { ValidatorException } = require("@exceptions");
const UserService = require("@services/user");

module.exports = {
  validators: {
    index: [],
  },

  /**
   * @api {get} /api/v1/users Show all users
   * @apiName ShowAllUsers
   * @apiGroup Users
   * @apiVersion 1.0.0
   *
   *
   * @apiSuccess (Success 200) {array} users
   */
   index: async (req, res) => {
    const validationErrors = validationResult(req);
    if (!validationErrors.isEmpty()) {
      throw new ValidatorException(validationErrors);
    }

    const payload = await new UserService.ShowAll().execute();

    res.status(200).json(payload);
  },
};
