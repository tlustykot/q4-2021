/** @format */

module.exports = {
  isObjEmpty: function (obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  },
  objectToArray: function (obj) {
    let bigArray = [];
    Object.keys(obj).forEach(function (arr) {
      obj[arr].forEach(function (elem) {
        bigArray.push(elem);
      });
    });
    return bigArray;
  },
  delArrObjProperty: function (arr, property) {
    arr.forEach((object) => {
      delete object[property];
    });
    return arr;
  },
  sortArrayByProperty(arr, property, order) {
    arr.sort(function (a, b) {
      if (order == "desc") {
        return b[property] - a[property];
      }
      if (order == "asc") {
        return a[property] - b[property];
      }
    });
    return arr;
  },
  textToArray(text) {
    let txtArr = text.split("\n");
    return txtArr;
  },
  findInArray(arr, elem) {
    if (arr.indexOf(elem) !== -1) {
      return true;
    } else {
      return false;
    }
  },
  getRandomInteger(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  },
  getRandomHash(len) {
    let maxlen = 8,
      min = Math.pow(16, Math.min(len, maxlen) - 1);
    (max = Math.pow(16, Math.min(len, maxlen)) - 1),
      (n = Math.floor(Math.random() * (max - min + 1)) + min),
      (r = n.toString(16));
    while (r.length < len) {
      r = r + module.exports.getRandomHash(len - maxlen);
    }
    return r;
  },
  getRandomCode() {
    return Math.random().toString(36).substring(2, 9).toUpperCase();
  },
  handleCatchError(err, reject) {
    if (typeof err.statusCode !== "undefined") {
      return reject({
        statusCode: err.statusCode,
        statusInfo: err.statusInfo,
        statusDetail: err.statusDetail,
      });
    } else {
      return reject(err);
    }
  },
  processQueryResponse(res, rowArr, queryType, returnRow = true) {
    let emptyErrCode;
    let responseCode;
    switch (queryType) {
      case "insert":
        emptyErrCode = 400;
        responseCode = 201;
        if (rowArr.length === 0) {
          res.status(emptyErrCode).send();
        } else if (rowArr.length === 1 && returnRow) {
          res.status(responseCode).send(rowArr[0]);
        } else if (rowArr.length === 1 && !returnRow) {
          res
            .status(responseCode)
            .send({ statusCode: responseCode, statusDetail: "insert successful" });
        } else {
          res.status(responseCode).send(rowArr);
        }
        break;
      case "delete":
        emptyErrCode = 400;
        responseCode = 200;
        if (rowArr.length === 0) {
          res.status(emptyErrCode).send();
        } else if (rowArr.length === 1) {
          res.status(responseCode).send(rowArr[0]);
        } else {
          res.status(responseCode).send(rowArr);
        }
        break;
      case "update":
        emptyErrCode = 400;
        responseCode = 200;
        if (rowArr.length === 0) {
          res.status(200).send();
        } else if (rowArr.length === 1) {
          res.status(responseCode).send(rowArr);
        } else {
          res.status(responseCode).send(rowArr);
        }
        break;
      default:
        emptyErrCode = 200;
        responseCode = 200;
        if (rowArr.length !== 0) {
          res.status(responseCode).send(rowArr);
        } else {
          res.status(emptyErrCode).send(rowArr);
        }
    }
  },
  processPaymentResponse(res, response) {
    let emptyErrCode;
    let responseCode;
    emptyErrCode = 404;
    responseCode = 200;
    res.status(responseCode).send(response);
  },
  getTokenFromHeader(authorizationHeader) {
    let token = authorizationHeader.split(" ")[1];
    return token;
  },
  getTokenFromCookies(cookie) {
    let cookies = cookie.split(" ");
    let token;
    cookies.forEach((cookieElem) => {
      if (cookieElem.includes(TOKENCOOKIENAME)) {
        token = cookieElem.split("=")[1];
      }
    });
    return token;
  },
  validateCode(code, reqLength) {
    let lengthValid = false;
    let regexValid = false;
    var regex = /^[A-Za-z0-9]+$/;

    if (code.length === reqLength) {
      lengthValid = true;
    }
    if (regex.test(code)) {
      regexValid = true;
    }
    return lengthValid && regexValid;
  },
  query(dbPool, query, params) {
    return new Promise((resolve, reject) => {
      dbPool.poolInstance
        .connect()
        .then((client) => {
          return client
            .query(query, params)
            .then((queryRes) => {
              client.release();
              return resolve(queryRes);
            })
            .catch((err) => {
              client.release();
              let errObj = {
                statusCode: dbPool.dbQueryErrCode,
                statusInfo: dbPool.dbQueryError,
                statusDetail: err,
              };
              return reject(errObj);
            });
        })
        .catch((err) => {
          let errObj = {
            statusCode: dbPool.dbInternalErrCode,
            statusInfo: dbPool.dbInternalError,
            statusDetail: err,
          };
          return reject(errObj);
        });
    });
  },
  wrap(fn) {
    return async (req, res, next) => {
      // Catch any errors and pass them to the error handler
      // but also allow for adding a middleware after this one.
      try {
        // handle headersSent to skip processing immediately
        if (res.headersSent) {
          return next();
        }
        await fn(req, res);
        next();
      } catch (err) {
        next(err);
      }
    };
  },

  /**
   * Returns values for postgres query as $1,$2,$3...
   * @param {Object} params
   */
  getParamValues(params) {
    const length = Object.keys(params).length;
    return [...Array(length).keys()].map((e) => "$" + (e + 1)).join();
  },

  /**
   * Returns update values for postgres query as column_name = $value
   * @param {Object} params
   */
  getParamUpdate(params) {
    return Object.keys(params)
      .map((e, i) => e + " = $" + (i + 1))
      .join();
  },

  /**
   * Replaces {{}} with provided params
   * @param {String} template whole template as string, with {{param}} as variable to replace
   * @param {*} params object of params names to replace & their new values
   * @return {String} template with replaced params
   */
  replaceParams(template, params) {
    Object.keys(params).forEach((paramKey) => {
      if (paramKey == "contents" || paramKey == "footerText") {
        params[paramKey] = params[paramKey].replace(/\n/g, "<br/>");
      }
      template = template.replace(new RegExp(`{{${paramKey}}}`, "g"), params[paramKey]);
    });

    return template;
  },

  /**
   * Searches for #[localizedString](param)# and replaces it with provided params
   * @param {*} template
   * @param {*} params
   */
  replaceLocalizedParams(template, params) {
    const mainRegex = new RegExp(/\#\[[^\]]*\]\((.*?)\)\#/, "g");
    if (Object.keys(params).length == 0) {
      return template.replace(mainRegex, "");
    }

    Object.keys(params).forEach((paramKey) => {
      const regex = new RegExp(/\#\[[^\]]*\]\(/.source + paramKey + /\)\#/.source, "g");
      if (params[paramKey]) {
        template = template.replace(regex, params[paramKey]);
      } else {
        template = template.replace(regex, "");
      }
    });

    return template.replace(mainRegex, "");
  },

  promiseTimeout(ms, promise) {
    // Create a promise that rejects in <ms> milliseconds
    let timeout = new Promise((resolve, reject) => {
      let id = setTimeout(() => {
        clearTimeout(id);
        reject("Timed out in " + ms + "ms.");
      }, ms);
    });

    // Returns a race between our timeout and the passed in promise
    return Promise.race([promise, timeout]);
  },

  isColorDark(color) {
    // Variables for red, green, blue values
    var r, g, b, hsp;
    // Check the format of the color, HEX or RGB?
    if (color.match(/^rgb/)) {
      // If HEX --> store the red, green, blue values in separate variables
      color = color.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/);
      r = color[1];
      g = color[2];
      b = color[3];
    } else {
      // If RGB --> Convert it to HEX: http://gist.github.com/983661
      color = +("0x" + color.slice(1).replace(color.length < 5 && /./g, "$&$&"));
      r = color >> 16;
      g = (color >> 8) & 255;
      b = color & 255;
    }
    // HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html
    hsp = Math.sqrt(0.299 * (r * r) + 0.587 * (g * g) + 0.114 * (b * b));
    // Using the HSP value, determine whether the color is light or dark
    if (hsp > 157.5) {
      return false;
    } else {
      return true;
    }
  },

  /**
   * Builds path to the file
   *
   * @param {string} file.directory
   * @param {string} file.uuid
   * @param {string} file.name
   * @param {boolean} includeDirectory prefix the path with directory
   */
  buildFilePath(file, includeDirectory = true) {
    var path = includeDirectory ? file.directory + "/" : "";

    path += file.uuid.substring(0, 6).split("").join("/") + "/" + file.uuid + "/" + file.name;

    return path;
  },
};
