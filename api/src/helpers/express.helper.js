/** @format */

const express = require("express");
const changeCase = require("change-object-case");

module.exports = () => {
  express.response.snakeCaseJSON = function (data) {
    let snakeData;
    if (Array.isArray(data)) {
      snakeData = changeCase.snakeArray(data, { recursive: true, arrayRecursive: true });
    } else {
      snakeData = changeCase.snakeKeys(data, { recursive: true, arrayRecursive: true });
    }
    return this.json(snakeData);
  };
};
