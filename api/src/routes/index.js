/** @format */

module.exports = (app) => {
    app.use((req, _res, next) => {
        next();
    });

    require("./v1")(app);
};
