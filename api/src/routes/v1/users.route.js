/** @format */

const express = require("express");
const router = express.Router();
const { wrap } = require("@helpers/all.helper");
const users = require("@controllers/v1/user.controller");

const PREFIX = "/users";

/**
 *  @function GET /users
 */
router.get(PREFIX, users.validators.index, wrap(users.index));

module.exports = router;
