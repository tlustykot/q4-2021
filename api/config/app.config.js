/** @format */

const dotenv = require("dotenv");

dotenv.config();

exports = module.exports = appConfig;

function appConfig() {
  if (!(this instanceof appConfig)) return new appConfig();

  // MAIN APP CONFIG
  global.LOG_LEVEL = process.env.LOG_LEVEL || "trace";
  global.EXPRESSPORT = Number(process.env.PORT) || 8001;
  global.REQUEST_TIMEOUT = Number(process.env.REQTIMEOUT) || 60000;
  global.RESTART_DELAY = Number(process.env.RESTART_DELAY) || 1000;
  global.APPURL =
    process.env.APPURL !== undefined
      ? process.env.APPURL + ":" + EXPRESSPORT
      : "localhost:" + EXPRESSPORT;

  global.API_VERSION = "0.1";

  global.DEFAULT_MODEL_VERSION = "v1";
}
