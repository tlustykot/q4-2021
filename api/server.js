/** @format */

require("module-alias/register");
require("@config/app.config")();

const errorHandler = require("@middleware/error-handler.middleware");

const express = require("express"),
  bodyParser = require("body-parser"),
  http = require("http"),
  path = require("path");

const appendRoutes = require("@routes");

var app = express();
app.disable("x-powered-by");

app.use("/", express.static(path.join(__dirname, "view")));

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Authorization, Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setTimeout(REQUEST_TIMEOUT, function () {
    console.error("Freeze on request timeout, restarting...");
    setTimeout(() => {
      process.exit(1);
    }, RESTART_DELAY);
  });
  next();
});

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use(function (req, res, next) {
  console.log(req?.url);
  next();
});

// create http server for sockets
let httpserver = http.createServer(app);

// Logging requests' data
appendRoutes(app);

// Handling exceptions from @exceptions
app.use(errorHandler);

httpserver.listen(EXPRESSPORT, function () {
  console.log("API is running and listening on: %s", EXPRESSPORT);
});
