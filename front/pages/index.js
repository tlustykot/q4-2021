/** @format */

import React from "react";
import API from "../services/api";

function Page(props) {
  const { users } = props;

  return (
    <React.Fragment>
      {`${users.length} users fetched from API.`} <br />
      Everything is fine. Good luck.
    </React.Fragment>
  );
}

Page.getInitialProps = async () => {
  const api = new API();
  const users = await api.Users.getAll();
  return { users };
};

export default Page;
