function setCustomHeaders(ctx) {
  let res = ctx.res;

  if (res) {
    res.setHeader("Access-Control-Allow-Methods", "POST");
    res.setHeader(
      "Access-Control-Allow-Headers",
      "Authorization, Origin, X-Requested-With, Content-Type, Accept"
    );
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader("Referrer-Policy", "same-origin");
    res.setHeader(
      "Strict-Transport-Security",
      "max-age=15552000;  includeSubDomains"
    );
    res.setHeader("X-Content-Type-Options", "nosniff");
    res.setHeader("X-XSS-Protection", "1; mode=block");
    res.setHeader("X-Download-Options", "noopen");
    res.setHeader("X-DNS-Prefetch-Control", "off");

    // CONTENT SECURITY POLICY
    let scriptSrc = `script-src
            'self'
            'unsafe-eval'
            'unsafe-inline'
            https://www.gstatic.com
            https://www.google.com;`;

    let connectSrc = `connect-src
            'self'
            blob:
            data:
            ${process.env.NEXT_PUBLIC_API_HOST};`;

    const cspDirectives = scriptSrc + connectSrc;

    res.setHeader("Content-Security-Policy", cspDirectives.replace(/\n/g, " "));
  }
  return null;
}

export default setCustomHeaders;
