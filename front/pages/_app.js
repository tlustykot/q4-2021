import App from "next/app";
import setCustomHeaders from "./_headers";

function HRApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

HRApp.getInitialProps = async (appContext) => {
  const appProps = await App.getInitialProps(appContext);
  setCustomHeaders(appContext);
  return { ...appProps };
};

export default HRApp;
