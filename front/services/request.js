class HTTPException extends Error {
  constructor(requestResponse, ...params) {
    super(requestResponse, ...params);

    this.error = true;
    this.code = requestResponse.code;
    this.name = "API Request Error";
    this.message = requestResponse.body.message;
    this.details = requestResponse.body.details;
  }
}

const REQUEST_POST = "POST";
const REQUEST_GET = "GET";

class Request {
  constructor(req) {}

  async make(type, url, params, authObj) {
    let headers = {
      "Content-Type": "application/json",
    };

    if (authObj) {
      if (!authObj.username || !authObj.password) {
        return;
      }
      headers["Authorization"] = `Basic ${btoa(
        authObj.username + ":" + authObj.password
      )}`;
    }

    let body = type === REQUEST_POST ? JSON.stringify(params) : undefined;

    const response = await fetch(url, {
      method: type,
      headers: headers,
      body: body,
    });
    return this._processResponse(await this._parse(response));
  }

  async _parse(response) {
    let body;
    if (response && response.status !== 201) {
      try {
        body = await response.json();
      } catch (error) {
        console.error(
          "Could not process API response",
          response.status,
          response.url
        );
        throw new HTTPException(response);
      }
    }
    return {
      code: response.status,
      state: response.ok,
      body: body,
    };
  }

  _processResponse(response) {
    if (!response) {
      throw new HTTPException(response);
    }
    if (response.code === 200 || response.code === 201) {
      return response.body;
    } else {
      throw new HTTPException(response);
    }
  }

  async makeRequest(method, username, password, query) {
    return await this.make(method, query, {}, { username, password });
  }
}

export default Request;
