import Request from "../request";
const request = new Request();
const REQUEST_GET = "GET";

class Users {
  constructor() {}

  async getAll() {
    return await request.make(
      REQUEST_GET,
      process.env.NEXT_PUBLIC_API_HOST + `/api/v1/users`,
      {}
    );
  }
}

export default Users;
