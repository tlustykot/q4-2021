import Users from "./users";

class API {
  constructor(req) {
    this.req = req;
    this.Users = new Users();
  }
}

export default API;
